TODO: Перейти от путей /home/ubuntu/www ... к нормальным

Инструкция:
1. Утсанавливаем docker, docker-compose, certbot (letsencrypt) на машину и убеждаемся, что docker
daemon запускается автоматически
2. Логинимся в registry: `./gitlab-registry-login.sh`
3. Создаем в /home/ubuntu файл production.env с правильными настройками (пример production.env-example)
дан
4. Запускаем сертификат с помощью `certbot certonly`, подробнее в гугл
5. dhparam.pem нужно положить в /home/ubuntu/certs а также убедиться, что сертификаты letsencrypt лежат
в папке /etc/letsencrypt (именно она монтируется в docker-compose)
6. `docker-compose up -d`
7. `docker-compose run --rm web ./manage.py migrate`
8. `docker-compose run --rm web ./manage.py collectstatic`
9. Создаем новое правило в crontab для автоматического обновления сертификатов:
`sudo crontab -e` пишем туда

```30 2 * * 1 /home/ubuntu/certbot/certbot-auto renew >> /home/ubuntu/renew.log```

```35 2 * * 1 docker-compose -f /home/ubuntu/configs/production/docker-compose.yml restart nginx```
