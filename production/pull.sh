docker-compose pull
docker-compose scale web=0 worker=0
docker-compose scale worker=1 web=1
docker-compose restart nginx
docker-compose run --rm web ./manage.py migrate
docker-compose run --rm web ./manage.py collectstatic --clear --noinput
